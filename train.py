import argparse
from pathlib import Path
from models import *
from util import *

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, required=True, choices=M4MetaModel.get_registered_models().keys())
    parser.add_argument('--run', type=int, required=False, default=0)
    parser.add_argument('--features', type=str, required=True, choices=snakemake_features_list())
    parser.add_argument('--output_stats', type=str, required=False)
    parser.add_argument('--feature_correlation_threshold', type=float, required=False, default=1)
    
    args = parser.parse_args()
    
    print(f'Training with {args.model}, run {args.run}, dataset {args.features}, threshold {args.feature_correlation_threshold}')
    
    ntfm = get_problem_features([x.strip()+'.csv' for x in args.features.split(',')])
    pdf = get_problem_algorithm_performance()

    X_train, X_test, Y_train, Y_test = train_test_split(ntfm, pdf, test_size=0.2, random_state=args.run)
    
    if args.feature_correlation_threshold<1.0:
        drop_columns = set(find_correlation(X_train, thresh=args.feature_correlation_threshold))
        X_train = X_train.drop(columns=drop_columns)
        X_test = X_test.drop(columns=drop_columns)
        
    X_train, X_test, Y_train, Y_test = X_train.to_numpy(), X_test.to_numpy(), Y_train.to_numpy(), Y_test.to_numpy()
    
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    
    print(f'X_train: {X_train.shape}, X_test: {X_test.shape}, Y_train: {Y_train.shape}, Y_test: {Y_test.shape}')
    
    model_type = M4MetaModel.get_registered_models()[args.model]
    
    save_dir = f'models/trained/{args.model}_{args.run}_{args.features}_{args.feature_correlation_threshold}'

    try:
        model = model_type.load_model(save_dir)
        print('Model loaded')
    except:
        model = model_type()
        print('Model training start')
        model.fit(X_train, Y_train)
        print('Model training end')
        model.save(save_dir)
    
    stats = {}
    test_errors = model.error(X_test, Y_test)
    stats = {**stats, **{f'test_{k}': v for k, v in test_errors.items()}}
    
    train_errors = model.error(X_train, Y_train)
    stats = {**stats, **{f'train_{k}': v for k, v in train_errors.items()}}
    
    
    test_problem_errors=model.problem_errors(X_test, Y_test, column_names=pdf.columns)
    stats = {**stats, **{f'test_a_{k}': v for k, v in test_problem_errors.items()}}

    train_problem_errors = model.problem_errors(X_train, Y_train, column_names=pdf.columns)
    stats = {**stats, **{f'train_a_{k}': v for k, v in train_problem_errors.items()}}
    
    if args.output_stats is None:
        print(stats)
    else:
        stats_dir = f'models/stats'
        Path(stats_dir).mkdir(parents=True, exist_ok=True)
        print(f'Saving to: {args.output_stats}')
        pickle.dump(stats, open(args.output_stats, "wb"))
    
    
