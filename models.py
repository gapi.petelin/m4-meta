from abc import ABC
from abc import abstractmethod
from pathlib import Path
import pickle
import numpy as np

from sklearn.neural_network import MLPRegressor
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.dummy import DummyRegressor
from sklearn.multioutput import MultiOutputRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.model_selection import train_test_split

from tqdm import tqdm
from flaml import AutoML

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout

from xgboost import XGBRegressor


class M4MetaModel(ABC):
    _registry = {}

    def __init_subclass__(cls, is_registered=True, **kwargs):
        super().__init_subclass__(**kwargs)
        if is_registered:
            M4MetaModel._registry[cls.__name__] = cls

    @staticmethod
    def get_registered_models():
        return M4MetaModel._registry

    @staticmethod
    def get_registered_model(name):
        return M4MetaModel._registry[name]

    @abstractmethod
    def predict(self, X):
        pass

    @abstractmethod
    def fit(self, X, Y):
        pass

    @abstractmethod
    def save(self, path):
        pass
    
    def error(self, X, Y):
        errors = {}
        pred = self.predict(X)
        errors['mae'] = mean_absolute_error(Y, pred)
        errors['mse'] = mean_squared_error(Y, pred)
        return errors
    
    def problem_errors(self, X, Y, column_names=None):
        errors = {}
        pred = self.predict(X)
        if column_names is None:
            column_names = range(Y.shape[1])
        for y, p, i in zip(Y.T, pred.T, column_names):
            errors[f'mae_{i}'] = mean_absolute_error(Y, pred)
            errors[f'mse_{i}'] = mean_squared_error(Y, pred)
        return errors


class SkLearnModel(M4MetaModel, ABC, is_registered=False):
    @staticmethod
    def load_model(directory):
        return pickle.load(open(directory + '/model.p', "rb"))

    def __init__(self, model):
        self.model = model

    def predict(self, X):
        return self.model.predict(X)

    def fit(self, X, Y):
        self.model.fit(X, Y)

    def save(self, directory):
        Path(directory).mkdir(parents=True, exist_ok=True)
        pickle.dump(self, open(directory + '/model.p', "wb"))


class MLPRegressorModel(SkLearnModel):
    def __init__(self):
        model = MLPRegressor(hidden_layer_sizes=(100, 100))
        super().__init__(model)
        
class KNNRegressorModel(SkLearnModel):
    def __init__(self):
        model = KNeighborsRegressor(metric='minkowski')
        super().__init__(model)
        
class KNNCosineRegressorModel(SkLearnModel):
    def __init__(self):
        model = KNeighborsRegressor(metric='cosine')
        super().__init__(model)
        
class LinearRegressionModel(SkLearnModel, is_registered=False):
    def __init__(self):
        model = LinearRegression()
        super().__init__(model)
        
class DummyMeanRegressionModel(SkLearnModel):
    def __init__(self):
        model = DummyRegressor(strategy="mean")
        super().__init__(model)
        
class DummyMedianRegressionModel(SkLearnModel):
    def __init__(self):
        model = DummyRegressor(strategy="median")
        super().__init__(model)
        
class XGBRegressionModel(SkLearnModel):
    def __init__(self):
        model = MultiOutputRegressor(XGBRegressor(n_estimators=100))
        super().__init__(model)
        

class FlamlAutoMLRegressionModel(M4MetaModel, is_registered=False):
    def __init__(self):
        super().__init__([])
        
    @staticmethod
    def load_model(directory):
        return pickle.load(open(directory + '/model.p', "rb"))

    def __init__(self, singletask_time_budget=1000):
        self.model = []
        self.singletask_time_budget = singletask_time_budget

    def predict(self, X):
        Yv = []
        for model in self.model:
            Yv.append(model.predict(X))
        return np.vstack(Yv).T

    def fit(self, X, Y):
        X_train, X_val, Y_train, Y_val = train_test_split(X, Y, test_size=0.05)
        for y, y_val in tqdm(list(zip(Y_train.T, Y_val.T))):
            model = AutoML(task="regression", time_budget=self.singletask_time_budget, metric='mae')
            model.fit(X_train, y, X_val=X_val, y_val=y_val)
            self.model.append(model)

    def save(self, directory):
        Path(directory).mkdir(parents=True, exist_ok=True)
        pickle.dump(self, open(directory + '/model.p', "wb"))
        
        
class KerasNetRegressionModel(M4MetaModel):
    @staticmethod
    def load_model(directory):
        model = KerasNetRegressionModel()
        from keras.models import load_model
        model.model = load_model(f'{directory}/model.h5')
        return model

    def __init__(self):
        self.model = None
        self.num_hidden_layers = 3
        self.hidden_layer_size = 500

    def predict(self, X):
        return self.model.predict(X)

    def fit(self, X, Y):
        self.model = Sequential()
        self.model.add(Dense(self.hidden_layer_size, input_shape=(X.shape[1],), activation="relu"))
        for li in range(self.num_hidden_layers):
            self.model.add(Dense(self.hidden_layer_size, activation="relu"))
        self.model.add(Dense(Y.shape[1], activation="relu"))
        
        self.model.compile(loss='mae', optimizer='adam')
        self.model.fit(X, Y, epochs=10, batch_size=128, verbose=False)

    def save(self, directory):
        Path(directory).mkdir(parents=True, exist_ok=True)
        self.model.save(f'{directory}/model.h5')
        
        
class KerasDropoutNetRegressionModel(M4MetaModel):
    @staticmethod
    def load_model(directory):
        model = KerasNetRegressionModel()
        from keras.models import load_model
        model.model = load_model(f'{directory}/model.h5')
        return model

    def __init__(self):
        self.model = None

    def predict(self, X):
        return self.model.predict(X)

    def fit(self, X, Y):
        self.model = Sequential()

        self.model.add(Dense(256, input_shape=(X.shape[1],), activation="relu"))
        self.model.add(Dropout(0.4))
        self.model.add(Dense(128, activation="relu"))
        self.model.add(Dense(Y.shape[1], activation="relu"))
        
        self.model.compile(loss='mae', optimizer='adam')
        #self.model.fit(X, Y, epochs=10, batch_size=128, verbose=False)
        
        self.model.fit(
            X,
            Y,
            epochs=50,
            batch_size=512,
            shuffle=True,
            #validation_data=(X_test, Y_test),
            verbose=False
        )

    def save(self, directory):
        Path(directory).mkdir(parents=True, exist_ok=True)
        self.model.save(f'{directory}/model.h5')
        