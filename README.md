# M4 Meta

## Setup notebook server

docker-compose up --build --force-recreate -d

## Snakemake

snakemake --cores all --keep-going
