from models import *
from util import *

rule all:
    input: expand("models/stats/m_{model}_r_{run}_f_{features}_t_{feature_correlation_threshold}.p", model=M4MetaModel.get_registered_models().keys(), run=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], features=snakemake_features_list(), feature_correlation_threshold=[1.0, 0.95, 0.9, 0.8, 0.7, 0.6])

rule train:
    output: "models/stats/m_{model}_r_{run}_f_{features}_t_{feature_correlation_threshold}.p"
    shell: "python train.py " \
                "--model {wildcards.model} " \
                "--run {wildcards.run} " \
                "--features {wildcards.features} " \
                "--feature_correlation_threshold {wildcards.feature_correlation_threshold} " \
                "--output_stats {output}"
