FROM continuumio/miniconda3
ADD environment.yaml environment.yaml
RUN apt-get --allow-releaseinfo-change update && apt-get install -y \
  unzip \
  curl \
  make \
  git

RUN conda install -c conda-forge -y mamba && \
    mamba env update --file environment.yaml --name root && \
    conda clean --all -y
