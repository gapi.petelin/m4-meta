import os
import glob
import pickle
import numpy as np
import pandas as pd
from sktime.performance_metrics.forecasting import mean_absolute_percentage_error, mean_absolute_error

def get_pair_errors(error_function=mean_absolute_percentage_error):
    from collections import namedtuple
    Performance = namedtuple('Performance', ['time_series', 'algorithm', 'error'])
    
    save_file_name = f'pair_errors_{error_function.__name__}.p'
    if os.path.isfile(save_file_name):
        pair_errors = pickle.load(open(save_file_name, 'rb'))
        return [Performance(x[0], x[1], x[2]) for x in pair_errors]
    else:
        test_set = dict()
        for f in glob.glob('data/test/*.csv'):
            df = pd.read_csv(f)
            for index, row in df.iterrows():
                ts_name, *ts_values = row.tolist()
                test_set[ts_name] = np.array(ts_values).astype(float)

        pair_errors = []

        for f in glob.glob('predictions/*.csv'):
            df = pd.read_csv(f)
            algo_name = f.split('-')[1].replace('.csv', '')
            print(algo_name)
            for index, row in df.iterrows():
                ts_name, *ts_values = row.tolist()
                ts_values = np.array(ts_values).astype(float)
                ts_values = ts_values[~np.isnan(ts_values)]
                error = error_function(test_set[ts_name], ts_values)
                pair_errors.append((ts_name, algo_name, error))
        pickle.dump(pair_errors, open(save_file_name, 'wb'))
        return [Performance(x[0], x[1], x[2]) for x in pair_errors]


def get_problem_algorithm_performance():
    errors = get_pair_errors()
    df = pd.DataFrame(errors, columns = ['Problem', 'Algorithm', 'Error'])
    pdf = df.pivot_table(values='Error', index='Problem', columns='Algorithm', aggfunc='first')
    pdf = pdf.reindex(sorted(pdf.columns), axis=1)
    return pdf


def get_problem_features(file='m4-tsfeatures.csv'):
    if isinstance(file, str):
        return pd.read_csv(file, index_col=0).sort_index()
    elif isinstance(file, list):
        dfs = [pd.read_csv(f, index_col=0) for f in file]
        return dfs[0].join(dfs[1:]).sort_index()
    else:
        raise Exception('File has to be a string or a list')
        
        
def find_correlation(df, thresh=0.9):
    """
    Given a numeric pd.DataFrame, this will find highly correlated features,
    and return a list of features to remove
    params:
    - df : pd.DataFrame
    - thresh : correlation threshold, will remove one of pairs of features with
               a correlation greater than this value
    """
    
    corrMatrix = df.corr()
    corrMatrix.loc[:,:] =  np.tril(corrMatrix, k=-1)

    already_in = set()
    result = []

    for col in corrMatrix:
        perfect_corr = corrMatrix[col][corrMatrix[col] > thresh].index.tolist()
        if perfect_corr and col not in already_in:
            already_in.update(set(perfect_corr))
            perfect_corr.append(col)
            result.append(perfect_corr)


    select_nested = [f[1:] for f in result]
    select_flat = [i for j in select_nested for i in j]
    return select_flat
            
def features_list():
    return ['m4-tsfeatures', 'm4-catch22features']

def snakemake_features_list():
    return ['m4-tsfeatures', 'm4-catch22features', 'm4-tsfeatures,m4-catch22features']